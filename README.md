<div align=center>

# Sub2Srs Search
> Mirrored from [**GitHub**/Ajatt-Tools/cropro](https://github.com/Ajatt-Tools/cropro)

[![Rate on AnkiWeb](https://glutanimate.com/logos/ankiweb-rate.svg)](https://ankiweb.net/shared/info/717309388)

Why having hundreds of thousands of cards on your computer, if you can have even more online?

This addon enables you to search and import all cards from [Immersion Kit](https://www.immersionkit.com/).

Based on [CroPro (Cross Profile Import) 2.0](https://ankiweb.net/shared/info/1772763629).

</div>

## Download
### [`717309388`](https://ankiweb.net/shared/info/717309388)
You can get the addon by going to `Tools` > `Add-ons` > `Get add-ons...` and pasting the code.

Else you can also grab the file from the [releases](https://codeberg.org/FileX/SubSearch/releases) and execute it (you will have to have anki installed already).

## Screenshots & How to use
<div align=center>
    <img src="https://codeberg.org/FileX/SubSearch/raw/branch/main/img/MainMenuScreen.png" width="1200"><br><br>

Install Sub2Srs Search and start Anki. You will see a menu called `Sub2Srs Search`.<br>
To search for cards, click `Search for Sub2Srs Cards...`.<br><br>

</div>

<div align=center>
    <img src="https://codeberg.org/FileX/SubSearch/raw/branch/main/img/FullCapabilities.png" width="1200"><br><br>

This will open a search window, including a lot of hopefully self explaining options.<br>
Looks a bit like cockpit-style, but no need to worry as you can turn this off in the settings or hide it per button.

Enter some text and search for it. Now you should roughly have the screenshot.

Click on one of the cards to preview it (Can be turned off in the settings). Multi-Selection is possible.

A bit below is the deck and the note type that will be used.<br>
Also, there is a notice of missing fields. Import can still be done, but these fields will be cut away.

Click edit to edit the notes details, import to simply import.<br>
</div>

> If you get a notice for existing media, check the card in the card-browser or editor.

> Some filters seem not yet supported by immersionkit but added to their documentation.<br>
**Those are left in SubSearch but may not work.**

<br><br>
<div align=center>
    <img src="https://codeberg.org/FileX/SubSearch/raw/branch/main/img/Settings.png" width="600"><br><br>

These displayed here are all settings available for SubSearch together with their descriptions.

See the [Config documentation](config.md) for more information/the information on the right.
</div>

---
> Warning: This addon can be a cause of losing interest in anime<br>
> The reasons for this are that you will most likely take any card you can get, thus also from anime you don't like, and you are being spoiled about the content of the anime you like on top.
> <br>Keep this in mind when using the addon.

## Links

* [Anki Forum Page](https://forums.ankiweb.net/t/sub2srs-search-official-thread/38034)
* [Anki Addons Page](https://ankiweb.net/shared/info/717309388)
* [Immersion Kit API Documentation](https://docs.immersionkit.com/public%20api/search/)
* [Config documentation](config.md)
* [Source code of CroPro](https://github.com/Ajatt-Tools/cropro/)
