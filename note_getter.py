import json
from urllib import request, parse
import re
space_pattern = re.compile(r'\s+')
multi_space_pattern = re.compile(r'\s\s+')

from aqt import mw
from aqt.utils import showInfo

kakasi = False
# Is being called upon anki startup from searchwindows init
def import_kakasi(r):
    if r and config["jlab_format"]:
        from .pykakasi import src as pykakasi
        global kakasi
        kakasi = pykakasi.Kakasi()

from .common import LogDebug
from .config import config

logDebug = LogDebug()

cur_note_list = []


def get_for(url, term: str, *, extended_filters: list[str, str, list[str], list[int, int], str, str]) -> list[dict] or IOError:
    """extended_filters = [category (0), sort (1), tags (2), length_border (3), jlpt (4), wanikani (5)]"""
    # build URL out of secured values
    if term: term = parse.quote(term)
    if extended_filters[0]: category = extended_filters[0].lower()
    if extended_filters[1]: sort = extended_filters[1].lower()
    if extended_filters[2]: tags = ','.join(parse.quote(tag.replace(" ", "+").encode("utf8")) for tag in extended_filters[2])
    if extended_filters[3]:
        min_len = str(extended_filters[3][0])
        max_len = str(extended_filters[3][1])
    if extended_filters[4] and extended_filters[4] != "--": jlpt = extended_filters[4][1:]
    if extended_filters[5] and extended_filters[5] != "--": wanikani = extended_filters[5][6:]

    encoded_url = (f'{url}?keyword={term}{"" if extended_filters[0] == "--" else ("&category=" + category)}'
                   f'{"" if not extended_filters[1] else ("&sort=" + sort)}{"" if not extended_filters[2] else ("&tags=" + tags)}'
                   f'{"" if not extended_filters[3][0] else ("&min_length=" + min_len)}{"" if not extended_filters[3][1] else ("&max_length=" + max_len)}'
                   f'{"" if extended_filters[4] == "--" else ("&jlpt=" + jlpt)}{"" if extended_filters[5] == "--" else ("&wk=" + wanikani)}')

    logDebug(f'Getting card data for - {term}, {extended_filters[0]}, {extended_filters[1]}, {extended_filters[2]}, '
             f'{extended_filters[3]}, {extended_filters[4]}, {extended_filters[5]} ({encoded_url})')
    try:
        response = request.urlopen(encoded_url).read()
        result = json.loads(response)['data'][0]
    except IOError as err:
        return err

    global cur_note_list
    cur_note_list = []

    logDebug('Formatting fetched card data')

    # build up note dict. Simplified and no note, so it is easier to handle and not as big as a note
    for card in result['examples']:
        tags = card['tags']
        tags.append(card['category'])
        new_note = {
            'Expression': card['sentence'],
            'ID': card['sentence_id'],
            'Reading': card['sentence_with_furigana'],
            'English': card['translation'],
            'tags': tags,
            'source_info': f'{card["deck_name"]} {("("+card["deck_name_japanese"]+")") if card["deck_name_japanese"] else ""}'
                           f'{(", Episode "+str(card["episode"])) if card["episode"] else ""}',
            'needed_media': []
        }

        # UI and functionality combination
        if config['fetch_anki_card_media']:
            new_note["Audio"] = f'''<audio id="subsearch_{card["sentence_id"].replace('"', "'") +"."+ card["sound_url"].split(".")[-1]}_player" class="subsearch__fetch" src="{parse.quote(card['sound_url'], safe=":/%")}"></audio>
[sound:{parse.quote(card["sound_url"].replace("]", "］"), safe=":/%")}]'''
        else:
            new_note['Audio'] = f'[sound:{card["sentence_id"].replace("]", "］") +"."+ card["sound_url"].split(".")[-1]}]'
            new_note['needed_media'].append(card['sound_url'])

        # if card has no image don't add one
        # also replace " with ' against image destruction
        if card['image_url'] and config['fetch_anki_card_media']:
            new_note['Image'] = '<img src="'+card['image_url'].replace('"', "'")+'"/>'
        elif card['image_url']:
            new_note['Image'] = '<img src="'+card["sentence_id"].replace('"', "'")+f'.{card["image_url"].split(".")[-1]}"/>'
            new_note['needed_media'].append(card['image_url'])

        if config['jlab_format'] and kakasi:
            kakasi_conv = kakasi.convert(card['sentence'])
            # jlab takes the cloze position by splitting the string at spaces
            # "  " = wanted_position + 1, if you say it programmatically
            new_note['Jlab-Kanji'] = re.sub(multi_space_pattern, ' ', card['sentence'])
            new_note['Jlab-KanjiSpaced'] = re.sub(multi_space_pattern, ' ', " ".join(''.join(e for e in word["orig"] if e.isalnum() or e in ['[',']']) for word in kakasi_conv).replace("]", " ]"))
            new_note['Jlab-Hiragana'] = re.sub(multi_space_pattern, ' ', " ".join(''.join(e for e in word["hira"] if e.isalnum() or e in ['[',']']) for word in kakasi_conv).replace("]", " ]"))
            new_note['Jlab-KanjiCloze'] = new_note["Jlab-KanjiSpaced"]
            # try replacing the searched word with it's deinflected form provided by the api
            # not yet got any better method for this
            new_note['Jlab-Lemma'] = new_note["Jlab-KanjiSpaced"] if len(result["dictionary"]) == 0 or len(result["dictionary"][0]) == 0 or result["dictionary"][0][0]["headword"] == parse.unquote(term) else new_note["Jlab-KanjiSpaced"].replace(card["word_list"][card["word_index"][0]], result["dictionary"][0][0]['headword'])
            new_note['Jlab-HiraganaCloze'] = new_note["Jlab-Hiragana"]
            new_note['Jlab-Translation'] = card['translation']
            new_note['Jlab-DictionaryLookup'] = ''
            new_note['Jlab-Metadata'] = ''
            new_note['Jlab-Remarks'] = ''
            new_note['Other-Front'] = new_note["Jlab-KanjiCloze"]
            # surprise, jlab is not needed.. (wasted a lot of hours on this). Jlab needs only the hiragana cloze and kanji matching each other
            # shown roumaji are converted by them and displayed
            new_note['Jlab-ListeningFront'] = re.sub(multi_space_pattern, ' ', " ".join(''.join(e for e in word["hepburn"] if e.isalnum() or e in ['[',']']) for word in kakasi_conv).replace("]", " ]"))
            new_note['Jlab-ListeningBack'] = new_note['Jlab-ListeningFront']
            new_note['Jlab-ClozeFront'] = new_note['Jlab-ListeningFront']
            new_note['Jlab-ClozeBack'] = new_note['Jlab-ListeningFront']

        cur_note_list.append(new_note)

    return cur_note_list


def update_jlab_cards(col):
    if not config['jlab_format']: return True
    if not kakasi: return False
    
    for model in col.models.all_names_and_ids():
        if model.name.startswith('SubCard-JlabConverted'):
            model = mw.col.models.get(model.id)
            if len(model['tmpls']) > 0 and len(tmpl_filter := list(filter(lambda v: v['name'] == 'SubCard-Front-Back', model['tmpls']))) > 0:
                logDebug("1.2.0 convert for Jlab with model "+model['id'])
                model['tmpls'][model['tmpls'].index(tmpl_filter[0])]['name'] = "Jlab-ListeningCard"
                col.models.update_dict(model)
            
            for n_id in col.models.nids(model['id']):
                note = col.get_note(n_id)
                logDebug(f"1.2.0 convert for Jlab with {note.id}")
                try:
                    kakasi_conv = kakasi.convert(note['Expression'])
                    note['Jlab-Kanji'] = re.sub(multi_space_pattern, ' ', note['Expression'])
                    note['Jlab-KanjiSpaced'] = re.sub(multi_space_pattern, ' ', " ".join(''.join(e for e in word["orig"] if e.isalnum() or e in ['[',']']) for word in kakasi_conv).replace("]", " ]"))
                    note['Jlab-Hiragana'] = re.sub(multi_space_pattern, ' ', " ".join(''.join(e for e in word["hira"] if e.isalnum() or e in ['[',']']) for word in kakasi_conv).replace("]", " ]"))
                    note['Jlab-KanjiCloze'] = note["Jlab-KanjiSpaced"]
                    note['Jlab-Lemma'] = note["Jlab-KanjiSpaced"]
                    note['Jlab-HiraganaCloze'] = note["Jlab-Hiragana"]
                    note['Jlab-Translation'] = note['English']
                    note['Other-Front'] = note["Jlab-KanjiCloze"]
                    note['Jlab-ListeningFront'] = re.sub(multi_space_pattern, ' ', " ".join(''.join(e for e in word["hepburn"] if e.isalnum() or e in ['[',']']) for word in kakasi_conv).replace("]", " ]"))
                    note['Jlab-ListeningBack'] = note['Jlab-ListeningFront']
                    note['Jlab-ClozeFront'] = note['Jlab-ListeningFront']
                    note['Jlab-ClozeBack'] = note['Jlab-ListeningFront']
                except AttributeError:
                    showInfo("Fields seem corrupted on ID "+note['ID'])
                    continue

                col.update_note(note)
                yield n_id

    mw.reset()
